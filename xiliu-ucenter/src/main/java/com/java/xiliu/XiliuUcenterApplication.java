package com.java.xiliu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XiliuUcenterApplication {

	public static void main(String[] args) {
		SpringApplication.run(XiliuUcenterApplication.class, args);
	}

}
