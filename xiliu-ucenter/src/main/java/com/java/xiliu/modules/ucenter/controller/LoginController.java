package com.java.xiliu.modules.ucenter.controller;

import com.java.xiliu.common.R;
import com.java.xiliu.common.ResultCodeEnum;
import com.java.xiliu.common.service.CaptchaService;
import com.java.xiliu.common.untils.CaptchaUtils;
import com.java.xiliu.common.untils.SecurityUtils;
import com.java.xiliu.modules.ucenter.entity.XlMenu;
import com.java.xiliu.modules.ucenter.entity.XlUser;
import com.java.xiliu.modules.ucenter.service.LoginService;
import com.java.xiliu.modules.ucenter.service.XlMenuService;
import com.java.xiliu.modules.ucenter.service.XlUserService;
import com.java.xiliu.modules.ucenter.vo.LoginVo;
import com.java.xiliu.modules.ucenter.vo.RouterVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author xiliu
 * @description
 * @date 2022/6/30
 */
@Api(tags = "登录管理")
@RestController
public class LoginController {

    @Value("${jwt.tokenHead}")
    private String tokenHead;
    @Value(value = "${xl.login.authcode.enable}")
    private boolean needAuthCode;
    @Autowired
    private LoginService loginService;
    @Autowired
    private XlMenuService menuService;
    @Autowired
    private CaptchaService captchaService;

    @ApiOperation(value = "登录")
    @PostMapping(value = "login")
    public R login(@RequestBody LoginVo loginVo) {
        // 只有开启了验证码功能才需要验证
        if (needAuthCode) {
            String msg = captchaService.checkImageCode(loginVo.getNonceStr(),loginVo.getValue());
            if (StringUtils.isNotBlank(msg)) {
                return R.error(msg);
            }
        }
        String token = loginService.login(loginVo.getUserName(), loginVo.getPassWord());
        if (StringUtils.isBlank(token)) {
            return R.error("用户名或密码错误");
        }
        Map<String, String> tokenMap = new HashMap<>();
        tokenMap.put("token", token);
        tokenMap.put("tokenHead", tokenHead);
        return R.ok(tokenMap);
    }

    @ApiOperation(value = "登录后获取用户信息")
    @GetMapping("get-info")
    public R getInfo() {
        XlUser user = SecurityUtils.getLoginUser().getUser();
        if (user == null) {
            return R.unauthorized();
        }
        // 权限集合
        Set<String> permissions = loginService.getMenuPermission(user);
        // 角色集合
        Set<String> roles = loginService.getRolePermission(user);
        Map<String, Object> data = new HashMap<>();
        data.put("userCode", user.getUserCode());
        data.put("name", user.getRealName());
        data.put("avatar", user.getAvatar());
        data.put("roles", roles);
        data.put("permissions", permissions);
        return R.ok(data);
    }

    @ApiOperation(value = "登录后获取路由信息")
    @GetMapping("get-routers")
    public R getRoutes() {
        List<XlMenu> menus = menuService.selectMenuTreeByUserId(SecurityUtils.getUserId());
        List<RouterVo> routerVos = menuService.buildMenus(menus);
        return R.ok(routerVos);
    }
}
