package com.java.xiliu.modules.monitor.service.impl;

import com.java.xiliu.common.entity.JwtUser;
import com.java.xiliu.common.untils.StringUtils;
import com.java.xiliu.modules.monitor.entity.UserOnline;
import com.java.xiliu.modules.monitor.service.UserOnlineService;
import org.springframework.stereotype.Service;

/**
 * @author huoqiang
 * @description
 * @date 2022/10/18
 */
@Service
public class UserOnlineServiceImpl implements UserOnlineService{

    @Override
    public UserOnline selectOnlineByInfo(String ipaddr, String userName, JwtUser user) {
        if (StringUtils.equals(ipaddr, user.getIpaddr()) && StringUtils.equals(userName, user.getUsername())) {
            return setUserOnline(user);
        }
        return null;
    }

    @Override
    public UserOnline selectOnlineByIpaddr(String ipaddr, JwtUser user) {
        if (StringUtils.equals(ipaddr, user.getIpaddr())) {
            return setUserOnline(user);
        }
        return null;
    }

    @Override
    public UserOnline selectOnlineByUserName(String userName, JwtUser user) {
        if (StringUtils.equals(userName, user.getUsername())) {
            return setUserOnline(user);
        }
        return null;
    }

    @Override
    public UserOnline setUserOnline(JwtUser user) {
        if (StringUtils.isNull(user) || StringUtils.isNull(user.getUser())) {
            return null;
        }
        UserOnline userOnline = new UserOnline();
        userOnline.setTokenId(user.getToken());
        userOnline.setUserName(user.getUsername());
        userOnline.setIpaddr(user.getIpaddr());
        userOnline.setLoginLocation(user.getLoginLocation());
        userOnline.setBrowser(user.getBrowser());
        userOnline.setOs(user.getOs());
        userOnline.setLoginTime(user.getLoginTime());
        if (StringUtils.isNotNull(user.getUser().getDept())) {
            userOnline.setDeptName(user.getUser().getDept().getDeptName());
        }
        return userOnline;
    }
}
