package com.java.xiliu.modules.ucenter.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 用户表
 * </p>
 * @author xiliu
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="XlUser对象", description="用户表")
public class XlUser implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "用户ID")
    @TableId(value = "user_id", type = IdType.AUTO)
    private Long userId;

    @ApiModelProperty(value = "用户类型;1:admin;2:会员")
    private Integer userType;

    @ApiModelProperty(value = "性别 0：未知；1：男；2：女")
    private Integer sex;

    @ApiModelProperty(value = "出生年月")
    private Date birthday;

    @ApiModelProperty(value = "用户积分")
    private Integer score;

    @ApiModelProperty(value = "金币")
    private Integer coin;

    @ApiModelProperty(value = "余额")
    private BigDecimal balance;

    @ApiModelProperty(value = "用户状态;0:禁用,1:正常,2:未验证")
    private Integer userStatus;

    @ApiModelProperty(value = "用户名")
    private String userCode;

    @ApiModelProperty(value = "密码")
    private String passWord;

    @ApiModelProperty(value = "姓名")
    private String realName;

    @ApiModelProperty(value = "用户昵称")
    private String userNickname;

    @ApiModelProperty(value = "邮箱")
    private String userEmail;

    @ApiModelProperty(value = "用户头像")
    private String avatar;

    @ApiModelProperty(value = "个性签名")
    private String signature;

    @ApiModelProperty(value = "中国手机不带国家代码，国际手机号格式为：国家代码-手机号")
    private String mobile;

    @ApiModelProperty(value = "扩展属性")
    private String more;

    @ApiModelProperty(value = "最后登录时间")
    private Date lastLoginTime;

    @ApiModelProperty(value = "最后登录ip")
    private String lastLoginIp;

    @ApiModelProperty(value = "微信openid")
    private String openId;

    @ApiModelProperty(value = "公众号openid")
    private String gzhOpenId;

    @ApiModelProperty(value = "用户unionid")
    private String unionId;

    @ApiModelProperty(value = "用户级别 1普通 2房管员")
    private Integer userLevel;

    @ApiModelProperty(value = "可发布数 包括免费和收费")
    private Integer nums;

    @ApiModelProperty(value = "平台管理，将收到相关提醒 0否 1是")
    private Integer isadmin;

    @ApiModelProperty(value = "所属部门id")
    private Long deptId;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "逻辑删除 1已删除 0未删除")
    private Boolean deleted;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人")
    private Long createdBy;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createdTime;

    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新时间")
    private Date updatedTime;

    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新人")
    private Long updatedBy;

    @ApiModelProperty(value = "数据版本")
    private Integer version;

    /** 角色对象 */
    @TableField(exist = false)
    private List<XlRole> roles;

    /** 角色组 */
    @TableField(exist = false)
    private Long[] roleIds;

    /** 部门对象 */
    @TableField(exist = false)
    private XlDept dept;
}
