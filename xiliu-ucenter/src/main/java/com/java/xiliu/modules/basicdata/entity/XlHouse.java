package com.java.xiliu.modules.basicdata.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.util.Date;
import com.java.xiliu.common.entity.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 房源表对象 xl_house
 * 
 * @author xiliu
 * @date 2022-11-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("xl_house")
public class XlHouse extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /** 房源ID */
    private Long houseId;

    /** 房源编号 */
    private String orderNo;

    /** 房源标题 */
    private String title;

    /** 房源类型 */
    private String type;

    /** 厅室 */
    private Long hall;

    /** 房间 */
    private Long room;

    /** 卫生间 */
    private Long toilet;

    /** 阳台 */
    private Long balcony;

    /** 厨房 */
    private Long kitchen;

    /** 房屋面积 */
    private Long area;

    /** 房屋朝向 */
    private String orienta;

    /** 小区ID */
    private Long villageId;

    /** 小区/公寓名称 */
    private String name;

    /** 小区/公寓地址 */
    private String address;

    /** 城区id */
    private Long zoneId;

    /** 附近地铁站ID */
    private String subway;

    /** 附近地铁站名称 */
    private String subwayName;

    /** 经度 */
    private String lng;

    /** 纬度 */
    private String lat;

    /** 地图所在地址 */
    private String mapAddress;

    /** 房屋配置 */
    private String config;

    /** 房屋亮点 */
    private String tags;

    /** 装修情况 */
    private String decoration;

    /** 房源状态 */
    private Long state;

    /** 可入住日期 */
    private String rzDate;

    /** 可看房时间 */
    private String viewTime;

    /** 房屋描述 */
    private String ramark;

    /** 租金详情 */
    private Long money;

    /** 支付方式 */
    private String payType;

    /** 租客要求 */
    private String requirement;

    /** 显示状态 1显示 2隐藏 */
    private Long isshow;

    /** 用户ID */
    private Long userid;

    /** 发布时间 */
    private Long date;

    /** 管理员ID */
    private Long adminId;

    /** 佣金数 */
    private String commission;

    /** 是否置顶 1置顶 2未置顶 */
    private Long istop;

    /** 最短起租月份 */
    private String startMonth;

    /** 最长可租月份 */
    private String endMonth;

    /** 短租费用 */
    private Long shortRent;

    /** 押金 */
    private Long deposit;

    /** 管理/服务费 */
    private Long serviceMoney;

    /** 是否收中介费 */
    private Long isagency;

    /** 中介费 */
    private Long agencyMoney;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "逻辑删除 1已删除 0未删除")
    private Boolean deleted;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人")
    private Long createdBy;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdTime;

    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updatedTime;

    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新人")
    private Long updatedBy;

    /** 数据版本 */
    private Long version;




}
