package com.java.xiliu.modules.ucenter.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.java.xiliu.modules.ucenter.entity.XlRoleMenu;

/**
 * <p>
 * 角色和菜单关联表 服务类
 * </p>
 *
 * @author xiliu
 * @since 2022-07-06
 */
public interface XlRoleMenuService extends IService<XlRoleMenu> {

}
