package com.java.xiliu.modules.basicdata.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.xiliu.modules.basicdata.entity.XlCity;


/**
 * 城市选择表Mapper接口
 * 
 * @author xiliu
 * @date 2022-09-13
 */
public interface XlCityMapper extends BaseMapper<XlCity>{
    /**
     * 查询城市选择表
     * @param cityId 城市选择表主键
     * @return 城市选择表
     */
    XlCity selectXlCityByCityId(Long cityId);

    /**
     * 查询城市选择表列表
     * @param xlCity 城市选择表
     * @return 城市选择表集合
     */
   List<XlCity> selectXlCityList(XlCity xlCity);

}
