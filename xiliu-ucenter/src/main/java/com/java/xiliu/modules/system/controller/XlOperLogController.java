package com.java.xiliu.modules.system.controller;

import com.java.xiliu.common.R;
import java.util.List;

import com.java.xiliu.common.annotation.Log;
import com.java.xiliu.common.controller.BaseController;
import com.java.xiliu.common.enums.BusinessType;
import com.java.xiliu.common.page.TableDataInfo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.java.xiliu.modules.system.entity.XlOperLog;
import com.java.xiliu.modules.system.service.IXlOperLogService;


/**
 * 操作日志表Controller
 * 
 * @author xiliu
 * @date 2022-07-22
 */
@RestController
@RequestMapping("/system/operlog")
public class XlOperLogController extends BaseController {

    @Autowired
    private IXlOperLogService xlOperLogService;

    /**
     * 查询操作日志表列表
     */
    @PreAuthorize("@customSs.hasPermi('system:operlog:list')")
    @GetMapping("/list")
    public TableDataInfo list(XlOperLog xlOperLog) {
        startPage();
        List<XlOperLog> list = xlOperLogService.selectXlOperLogList(xlOperLog);
        return getDataTable(list);
    }

    /**
     * 获取操作日志表详细信息
     */
    @PreAuthorize("@customSs.hasPermi('system:operlog:query')")
    @GetMapping(value = "/{operId}")
    public R getInfo(@PathVariable("operId") Long operId) {
        return R.ok(xlOperLogService.selectXlOperLogByOperId(operId));
    }

    /**
     * 新增操作日志表
     */
    @PreAuthorize("@customSs.hasPermi('system:operlog:create')")
    @Log(title = "操作日志表", businessType = BusinessType.INSERT)
    @PostMapping
    public R create(@RequestBody XlOperLog xlOperLog) {
        boolean success = xlOperLogService.create(xlOperLog);
        if (success) {
            return R.ok();
        }
        return R.error("添加失败");
    }

    /**
     * 修改操作日志表
     */
    @PreAuthorize("@customSs.hasPermi('system:operlog:edit')")
    @Log(title = "操作日志表", businessType = BusinessType.UPDATE)
    @PostMapping(value = "/update/{operId}")
    public R update(@PathVariable Long operId,  @RequestBody XlOperLog xlOperLog) {
        xlOperLog.setOperId(operId);
        boolean success = xlOperLogService.updateXlOperLog(xlOperLog);
        if (success) {
            return R.ok();
        }
        return R.error("修改失败");
    }

    /**
     * 删除操作日志表
     */
    @PreAuthorize("@customSs.hasPermi('system:operlog:remove')")
    @Log(title = "操作日志表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{operIds}")
    public R remove(@PathVariable Long[] operIds) {
        return R.ok(xlOperLogService.deleteXlOperLogByOperIds(operIds));
    }
}
