package com.java.xiliu.modules.ucenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.xiliu.modules.ucenter.entity.XlRoleMenu;

/**
 * <p>
 * 角色和菜单关联表 Mapper 接口
 * </p>
 *
 * @author xiliu
 * @since 2022-07-06
 */
public interface XlRoleMenuMapper extends BaseMapper<XlRoleMenu> {

}
