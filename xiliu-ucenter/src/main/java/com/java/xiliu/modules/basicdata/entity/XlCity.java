package com.java.xiliu.modules.basicdata.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.util.Date;
import com.java.xiliu.common.entity.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 城市选择表对象 xl_city
 * 
 * @author xiliu
 * @date 2022-09-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("xl_city")
public class XlCity extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /** 主键id */
    @ApiModelProperty("主键ID")
    @TableId(value = "city_id", type = IdType.AUTO)
    private Long cityId;

    /** 名称 */
    private String cityName;

    /** 是否显示 0不显示 1显示 */
    private Long isShow;

    /** 显示顺序 */
    private Long sortOrder;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "逻辑删除 1已删除 0未删除")
    private Boolean deleted;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人")
    private Long createdBy;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createdTime;

    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updatedTime;

    @TableField(fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新人")
    private Long updatedBy;

    /** 数据版本 */
    private Long version;




}
