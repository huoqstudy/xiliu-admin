package com.java.xiliu.modules.basicdata.controller;

import com.java.xiliu.common.controller.BaseController;
import com.java.xiliu.common.enums.BusinessType;
import com.java.xiliu.common.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import com.java.xiliu.common.annotation.Log;
import com.java.xiliu.modules.basicdata.entity.XlCity;
import com.java.xiliu.modules.basicdata.service.IXlCityService;
import io.swagger.annotations.Api;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.java.xiliu.modules.basicdata.entity.XlSubway;
import com.java.xiliu.modules.basicdata.service.IXlSubwayService;
import com.java.xiliu.common.page.TableDataInfo;


/**
 * 地铁信息表Controller
 * 
 * @author xiliu
 * @date 2022-09-16
 */
@Api(tags = "地铁管理")
@RestController
@RequestMapping("/basic/subway")
public class XlSubwayController extends BaseController {
    @Autowired
    private IXlSubwayService xlSubwayService;
    @Autowired
    private IXlCityService cityService;

    /**
     * 查询地铁信息表列表
     */
    @PreAuthorize("@customSs.hasPermi('basic:subway:list')")
    @GetMapping("/list")
    public R list(XlSubway xlSubway) {
        List<XlSubway> list = xlSubwayService.selectXlSubwayList(xlSubway);
        return R.ok(list);
    }

    /**
     * 获取地铁信息表详细信息
     */
    @PreAuthorize("@customSs.hasPermi('basic:subway:query')")
    @GetMapping(value = { "/", "/{subwayId}" })
    public R getInfo(@PathVariable(value = "subwayId", required = false) String subwayId) {
        Map<String,Object> map = new HashMap<>();
        // 城市集合
        List<XlCity> cityList = cityService.list();
        XlSubway subway = xlSubwayService.selectXlSubwayBySubwayId(subwayId);
        map.put("cityList",cityList);
        map.put("subway",subway);
        return R.ok(map);
    }

    /**
     * 新增地铁信息表
     */
    @PreAuthorize("@customSs.hasPermi('basic:subway:create')")
    @Log(title = "地铁信息表", businessType = BusinessType.INSERT)
    @PostMapping("/create")
    public R create(@RequestBody XlSubway xlSubway) {
        boolean success = xlSubwayService.create(xlSubway);
        if (success) {
            return R.ok();
        }
        return R.error("添加失败");
    }

    /**
     * 修改地铁信息表
     */
    @PreAuthorize("@customSs.hasPermi('basic:subway:edit')")
    @Log(title = "地铁信息表", businessType = BusinessType.UPDATE)
    @PostMapping(value = "/update/{subwayId}")
    public R update(@PathVariable Long subwayId,  @RequestBody XlSubway xlSubway) {
        xlSubway.setSubwayId(subwayId);
        boolean success = xlSubwayService.update(xlSubway);
        if (success) {
            return R.ok();
        }
        return R.error("修改失败");
    }

    /**
     * 删除地铁信息表
     */
    @PreAuthorize("@customSs.hasPermi('basic:subway:remove')")
    @Log(title = "地铁信息表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{subwayIds}")
    public R remove(@PathVariable String[] subwayIds) {
        return R.ok(xlSubwayService.deleteXlSubwayBySubwayIds(subwayIds));
    }
}
