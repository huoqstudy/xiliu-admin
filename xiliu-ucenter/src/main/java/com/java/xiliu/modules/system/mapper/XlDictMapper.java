package com.java.xiliu.modules.system.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.java.xiliu.modules.system.entity.XlDict;

/**
 * 字典表Mapper接口
 * 
 * @author xiliu
 * @date 2022-08-09
 */
public interface XlDictMapper extends BaseMapper<XlDict>{
    /**
     * 查询字典表
     * @param dictId 字典表主键
     * @return 字典表
     */
    XlDict selectXlDictByDictId(Long dictId);

    /**
     * 查询字典表列表
     * @param xlDict 字典表
     * @return 字典表集合
     */
   List<XlDict> selectXlDictList(XlDict xlDict);

}
