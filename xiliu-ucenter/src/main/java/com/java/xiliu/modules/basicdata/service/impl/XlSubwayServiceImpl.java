package com.java.xiliu.modules.basicdata.service.impl;

import java.util.List;
import java.util.Arrays;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.java.xiliu.modules.basicdata.mapper.XlSubwayMapper;
import com.java.xiliu.modules.basicdata.entity.XlSubway;
import com.java.xiliu.modules.basicdata.service.IXlSubwayService;

/**
 * 地铁信息表Service业务层处理
 * 
 * @author xiliu
 * @date 2022-09-16
 */
@Service
public class XlSubwayServiceImpl extends ServiceImpl<XlSubwayMapper, XlSubway> implements IXlSubwayService {
    @Autowired
    private XlSubwayMapper xlSubwayMapper;

    /**
     * 查询地铁信息表
     * @param subwayId 地铁信息表主键
     * @return 地铁信息表
     */
    @Override
    public XlSubway selectXlSubwayBySubwayId(String subwayId) {
        return xlSubwayMapper.selectXlSubwayBySubwayId(subwayId);
    }

    /**
     * 查询地铁信息表列表
     * @param xlSubway 地铁信息表
     * @return 地铁信息表
     */
    @Override
    public List<XlSubway> selectXlSubwayList(XlSubway xlSubway) {
        return xlSubwayMapper.selectXlSubwayList(xlSubway);
    }

    /**
     * 新增地铁信息表
     * @param xlSubway 地铁信息表
     * @return 结果
     */
    @Override
    public boolean create(XlSubway xlSubway) {
        return save(xlSubway);
    }

    /**
     * 修改地铁信息表
     * @param xlSubway 地铁信息表
     * @return 结果
     */
    @Override
    public boolean update(XlSubway xlSubway) {
        return updateById(xlSubway);
    }

    /**
     * 批量删除地铁信息表
     * @param subwayIds 需要删除的地铁信息表主键
     * @return 结果
     */
    @Override
    public int deleteXlSubwayBySubwayIds(String[] subwayIds) {
        return xlSubwayMapper.deleteBatchIds(Arrays.asList(subwayIds));
    }

    /**
     * 删除地铁信息表信息
     * @param subwayId 地铁信息表主键
     * @return 结果
     */
    @Override
    public int deleteXlSubwayBySubwayId(String subwayId) {
        return xlSubwayMapper.deleteById(subwayId);
    }
}
