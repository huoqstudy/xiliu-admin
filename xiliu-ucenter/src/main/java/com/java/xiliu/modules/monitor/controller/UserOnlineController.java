package com.java.xiliu.modules.monitor.controller;

import com.java.xiliu.common.R;
import com.java.xiliu.common.annotation.Log;
import com.java.xiliu.common.constant.CacheConstants;
import com.java.xiliu.common.controller.BaseController;
import com.java.xiliu.common.entity.JwtUser;
import com.java.xiliu.common.enums.BusinessType;
import com.java.xiliu.common.page.TableDataInfo;
import com.java.xiliu.common.redis.RedisCache;
import com.java.xiliu.common.untils.StringUtils;
import com.java.xiliu.modules.monitor.entity.UserOnline;
import com.java.xiliu.modules.monitor.service.UserOnlineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author huoqiang
 * @description 在线用户监控
 * @date 2022/10/18
 */
@RestController
@RequestMapping("/monitor/online")
public class UserOnlineController  extends BaseController {

    @Autowired
    private RedisCache redisCache;
    @Autowired
    private UserOnlineService userOnlineService;

    @PreAuthorize("@customSs.hasPermi('monitor:online:list')")
    @GetMapping("/list")
    public TableDataInfo list(String ip, String userName) {
        Collection<String> keys = redisCache.keys(CacheConstants.LOGIN_TOKEN_KEY + "*");
        List<UserOnline> userOnlineList = new ArrayList<>();
        for (String key : keys) {
            JwtUser user = redisCache.getCacheObject(key);
            if (StringUtils.isNotEmpty(ip) && StringUtils.isNotEmpty(userName)) {
                if (StringUtils.equals(ip, user.getIpaddr()) && StringUtils.equals(userName, user.getUsername())) {
                    userOnlineList.add(userOnlineService.selectOnlineByInfo(ip, userName, user));
                }
            } else if (StringUtils.isNotEmpty(ip)) {
                if (StringUtils.equals(ip, user.getIpaddr())) {
                    userOnlineList.add(userOnlineService.selectOnlineByIpaddr(ip, user));
                }
            } else if (StringUtils.isNotEmpty(userName) && StringUtils.isNotNull(user.getUser())) {
                if (StringUtils.equals(userName, user.getUsername())) {
                    userOnlineList.add(userOnlineService.selectOnlineByUserName(userName,user));
                }
            } else {
                userOnlineList.add(userOnlineService.setUserOnline(user));
            }
        }
        return getDataTable(userOnlineList);
    }

    /**
     * 强退用户
     */
    @PreAuthorize("@customSs.hasPermi('monitor:online:forceLogout')")
    @Log(title = "在线用户", businessType = BusinessType.FORCE)
    @DeleteMapping("/{tokenId}")
    public R forceLogout(@PathVariable String tokenId) {
        redisCache.deleteObject(CacheConstants.LOGIN_TOKEN_KEY + tokenId);
        return R.ok();
    }
}
