package com.java.xiliu.modules.ucenter.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.java.xiliu.modules.ucenter.entity.XlUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.java.xiliu.modules.ucenter.vo.UserQueryVo;

import java.util.List;

/**
 * <p>
 * 后台用户表 服务类
 * </p>
 *
 * @author xiliu
 * @since 2021-09-03
 */
public interface XlUserService extends IService<XlUser> {

    /**
     * 注册功能
     * @param user
     * @return XlUser
     */
    XlUser register(XlUser user);

    /**
     * 查询用户
     * @param userCode
     * @return XlUser
     */
    XlUser getUserByCode(String userCode);

    /**
     * 获取列表。分页
     * @param queryVo 查询参数
     * @return page
     */
    Page<XlUser> listMemberPage(UserQueryVo queryVo);

    /**
     * 校验用户名称是否唯一
     * @param userName 用户名称
     * @return 结果
     */
    String checkUserNameUnique(String userName);

    /**
     * 添加用户
     * @param user
     * @return boolean
     **/
    boolean create(XlUser user);

    /**
     * 修改用户
     * @param user
     * @return boolean
     **/
    boolean update(XlUser user);

    /**
     * 批量删除用户
     * @param ids
     * @return boolean
     **/
    boolean delete(List<Long> ids);

    /**
     * 通过用户ID查询用户
     * @param userId 用户ID
     * @return 用户对象信息
     */
    XlUser selectUserById(Long userId);

    /**
     * 根据用户ID查询用户所属角色组
     * @param userId 用户id
     * @return 结果
     */
    String selectUserRoleGroup(Long userId);

    /**
     * 修改用户头像
     * @param userId 用户id
     * @param avatar 头像地址
     * @return 结果
     */
    boolean updateUserAvatar(Long userId, String avatar);
}
