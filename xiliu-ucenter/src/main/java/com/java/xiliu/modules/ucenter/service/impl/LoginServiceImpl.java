package com.java.xiliu.modules.ucenter.service.impl;

import com.java.xiliu.common.constant.Constants;
import com.java.xiliu.common.entity.JwtUser;
import com.java.xiliu.common.manager.factory.AsyncFactory;
import com.java.xiliu.common.manager.AsyncManager;
import com.java.xiliu.common.untils.JwtTokenUtil;
import com.java.xiliu.common.untils.SecurityUtils;
import com.java.xiliu.modules.ucenter.entity.XlUser;
import com.java.xiliu.modules.ucenter.service.LoginService;
import com.java.xiliu.modules.ucenter.service.XlMenuService;
import com.java.xiliu.modules.ucenter.service.XlRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * @author xiliu
 * @description
 * @date 2022/7/6
 */
@Slf4j
@Service
public class LoginServiceImpl implements LoginService{

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private XlMenuService menuService;
    @Autowired
    private XlRoleService roleService;

    @Override
    public String login(String username, String password) {
        String token = null;
        try {
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            if (!passwordEncoder.matches(password, userDetails.getPassword())) {
                throw new BadCredentialsException("密码不正确");
            }
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);
            // 登录成功记录日志
            AsyncManager.me().execute(AsyncFactory.recordLoginLog(username, Constants.LOGIN_SUCCESS, "登录成功"));
            JwtUser jwtUser = (JwtUser) authentication.getPrincipal();
            token = jwtTokenUtil.createToken(jwtUser);
        } catch (AuthenticationException e) {
            log.warn("登录异常:{}", e.getMessage());
            // 登录失败记录日志
            AsyncManager.me().execute(AsyncFactory.recordLoginLog(username, Constants.LOGIN_FAIL, e.getMessage()));
        }
        return token;
    }

    @Override
    public Set<String> getMenuPermission(XlUser user) {
        Set<String> perms = new HashSet<String>();
        // 超级管理员拥有所有权限
        if (SecurityUtils.isAdmin(user.getUserId())) {
            perms.add("*:*:*");
        } else {
            perms = menuService.selectMenuPermsByUserId(user.getUserId());
        }
        return perms;
    }

    @Override
    public Set<String> getRolePermission(XlUser user) {
        Set<String> roles = new HashSet<String>();
        // 超级管理员拥有所有权限
        if (SecurityUtils.isAdmin(user.getUserId())) {
            roles.add("admin");
        } else {
            roles = roleService.selectRolePermissionByUserId(user.getUserId());
        }
        return roles;
    }
}
