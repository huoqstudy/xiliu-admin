package com.java.xiliu.modules.basicdata.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.java.xiliu.modules.basicdata.entity.XlZone;

/**
 * 城区信息表Service接口
 * 
 * @author xiliu
 * @date 2022-09-13
 */
public interface IXlZoneService extends IService<XlZone>{
    /**
     * 查询城区信息表
     * @param zoneId 城区信息表主键
     * @return 城区信息表
     */
    XlZone selectXlZoneByZoneId(String zoneId);

    /**
     * 查询城区信息表列表
     * @param xlZone 城区信息表
     * @return 城区信息表集合
     */
    List<XlZone> selectXlZoneList(XlZone xlZone);

    /**
     * 新增城区信息表
     * @param xlZone 城区信息表
     * @return 结果
     */
    boolean create(XlZone xlZone);

    /**
     * 修改城区信息表
     * @param xlZone 城区信息表
     * @return 结果
     */
    boolean update(XlZone xlZone);

    /**
     * 批量删除城区信息表
     * 
     * @param zoneIds 需要删除的城区信息表主键集合
     * @return 结果
     */
    int deleteXlZoneByZoneIds(String[] zoneIds);

    /**
     * 删除城区信息表信息
     * 
     * @param zoneId 城区信息表主键
     * @return 结果
     */
    int deleteXlZoneByZoneId(String zoneId);
}
