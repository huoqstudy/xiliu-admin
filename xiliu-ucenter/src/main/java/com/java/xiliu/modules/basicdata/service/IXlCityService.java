package com.java.xiliu.modules.basicdata.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.java.xiliu.modules.basicdata.entity.XlCity;


/**
 * 城市选择表Service接口
 * 
 * @author xiliu
 * @date 2022-09-13
 */
public interface IXlCityService extends IService<XlCity>{
    /**
     * 查询城市选择表
     * @param cityId 城市选择表主键
     * @return 城市选择表
     */
    XlCity selectXlCityByCityId(Long cityId);

    /**
     * 查询城市选择表列表
     * @param xlCity 城市选择表
     * @return 城市选择表集合
     */
    List<XlCity> selectXlCityList(XlCity xlCity);

    /**
     * 新增城市选择表
     * @param xlCity 城市选择表
     * @return 结果
     */
    boolean create(XlCity xlCity);

    /**
     * 修改城市选择表
     * @param xlCity 城市选择表
     * @return 结果
     */
    boolean update(XlCity xlCity);

    /**
     * 批量删除城市选择表
     * 
     * @param cityIds 需要删除的城市选择表主键集合
     * @return 结果
     */
    int deleteXlCityByCityIds(Long[] cityIds);

    /**
     * 删除城市选择表信息
     * 
     * @param cityId 城市选择表主键
     * @return 结果
     */
    int deleteXlCityByCityId(Long cityId);
}
