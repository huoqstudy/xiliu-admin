package com.java.xiliu.modules.basicdata.service.impl;

import java.util.List;
import java.util.Arrays;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.java.xiliu.modules.basicdata.mapper.XlVillageMapper;
import com.java.xiliu.modules.basicdata.entity.XlVillage;
import com.java.xiliu.modules.basicdata.service.IXlVillageService;

/**
 * 小区表Service业务层处理
 * 
 * @author xiliu
 * @date 2022-09-16
 */
@Service
public class XlVillageServiceImpl extends ServiceImpl<XlVillageMapper, XlVillage> implements IXlVillageService {
    @Autowired
    private XlVillageMapper xlVillageMapper;

    /**
     * 查询小区表
     * @param villageId 小区表主键
     * @return 小区表
     */
    @Override
    public XlVillage selectXlVillageByVillageId(Long villageId) {
        return xlVillageMapper.selectXlVillageByVillageId(villageId);
    }

    /**
     * 查询小区表列表
     * @param xlVillage 小区表
     * @return 小区表
     */
    @Override
    public List<XlVillage> selectXlVillageList(XlVillage xlVillage) {
        return xlVillageMapper.selectXlVillageList(xlVillage);
    }

    /**
     * 新增小区表
     * @param xlVillage 小区表
     * @return 结果
     */
    @Override
    public boolean create(XlVillage xlVillage) {
        return save(xlVillage);
    }

    /**
     * 修改小区表
     * @param xlVillage 小区表
     * @return 结果
     */
    @Override
    public boolean update(XlVillage xlVillage) {
        return updateById(xlVillage);
    }

    /**
     * 批量删除小区表
     * @param villageIds 需要删除的小区表主键
     * @return 结果
     */
    @Override
    public int deleteXlVillageByVillageIds(Long[] villageIds) {
        return xlVillageMapper.deleteBatchIds(Arrays.asList(villageIds));
    }

    /**
     * 删除小区表信息
     * @param villageId 小区表主键
     * @return 结果
     */
    @Override
    public int deleteXlVillageByVillageId(Long villageId) {
        return xlVillageMapper.deleteById(villageId);
    }
}
