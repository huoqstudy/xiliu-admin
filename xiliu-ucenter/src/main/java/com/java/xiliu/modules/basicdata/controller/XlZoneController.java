package com.java.xiliu.modules.basicdata.controller;

import com.java.xiliu.common.controller.BaseController;
import com.java.xiliu.common.enums.BusinessType;
import com.java.xiliu.common.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import com.java.xiliu.common.annotation.Log;
import com.java.xiliu.modules.basicdata.entity.XlCity;
import com.java.xiliu.modules.basicdata.service.IXlCityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.java.xiliu.modules.basicdata.entity.XlZone;
import com.java.xiliu.modules.basicdata.service.IXlZoneService;
import com.java.xiliu.common.page.TableDataInfo;


/**
 * 城区信息表Controller
 * 
 * @author xiliu
 * @date 2022-09-13
 */
@Api(tags = "城区管理")
@RestController
@RequestMapping("/basic/zone")
public class XlZoneController extends BaseController {
    @Autowired
    private IXlZoneService xlZoneService;
    @Autowired
    private IXlCityService cityService;

    /**
     * 查询城区信息表列表
     */
    @PreAuthorize("@customSs.hasPermi('basic:zone:list')")
    @GetMapping("/list")
    public R list(XlZone xlZone) {
        List<XlZone> list = xlZoneService.selectXlZoneList(xlZone);
        return R.ok(list);
    }

    /**
     * 获取城区信息表详细信息
     */
    @PreAuthorize("@customSs.hasPermi('basic:zone:query')")
    @GetMapping(value = { "/", "/{zoneId}" })
    public R getInfo(@PathVariable(value = "zoneId", required = false) String zoneId) {
        Map<String,Object> map = new HashMap<>();
        // 城市集合
        List<XlCity> cityList = cityService.list();
        XlZone zone = xlZoneService.selectXlZoneByZoneId(zoneId);
        map.put("cityList",cityList);
        map.put("zone",zone);
        return R.ok(map);
    }

    /**
     * 新增城区信息表
     */
    @PreAuthorize("@customSs.hasPermi('basic:zone:create')")
    @Log(title = "城区信息表", businessType = BusinessType.INSERT)
    @PostMapping("/create")
    public R create(@RequestBody XlZone xlZone) {
        boolean success = xlZoneService.create(xlZone);
        if (success) {
            return R.ok();
        }
        return R.error("添加失败");
    }

    /**
     * 修改城区信息表
     */
    @PreAuthorize("@customSs.hasPermi('basic:zone:edit')")
    @Log(title = "城区信息表", businessType = BusinessType.UPDATE)
    @PostMapping(value = "/update/{zoneId}")
    public R update(@PathVariable Long zoneId,  @RequestBody XlZone xlZone) {
        xlZone.setZoneId(zoneId);
        boolean success = xlZoneService.update(xlZone);
        if (success) {
            return R.ok();
        }
        return R.error("修改失败");
    }

    /**
     * 删除城区信息表
     */
    @PreAuthorize("@customSs.hasPermi('basic:zone:remove')")
    @Log(title = "城区信息表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{zoneIds}")
    public R remove(@PathVariable String[] zoneIds) {
        return R.ok(xlZoneService.deleteXlZoneByZoneIds(zoneIds));
    }


    @ApiOperation("修改状态")
    @PostMapping("/update-status/{id}")
    public R updateStatus(@PathVariable Long id, @RequestParam(value = "status") String status) {
        XlZone xlZone = new XlZone();
        xlZone.setZoneId(id);
        xlZone.setStatus(status);
        boolean success = xlZoneService.updateById(xlZone);
        if (success) {
            return R.ok();
        }
        return R.error("修改失败");
    }
}
