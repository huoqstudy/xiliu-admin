package com.java.xiliu.common.entity;

import java.io.Serializable;

/**
 * @author xiliu
 * @description 基类
 * @date 2022/7/14
 */
public class BaseEntity extends BasePageEntity implements Serializable {
}
