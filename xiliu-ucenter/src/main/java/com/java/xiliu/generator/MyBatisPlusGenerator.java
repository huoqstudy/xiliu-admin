package com.java.xiliu.generator;

import cn.hutool.core.util.StrUtil;
import cn.hutool.setting.dialect.Props;

import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.LikeTable;
import com.baomidou.mybatisplus.generator.config.querys.MySqlQuery;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.VelocityTemplateEngine;

import java.util.Collections;
import java.util.Scanner;

/**
 * @author huoqiang
 * @description MyBatisPlus代码生成器
 * @date 2022/6/30
 */
public class MyBatisPlusGenerator {

    public static void main(String[] args) {
        // 1、创建代码生成器
        AutoGenerator autoGenerator = new AutoGenerator(initDataSourceConfig());
        // 2、全局配置
        String projectPath = System.getProperty("user.dir");
        autoGenerator.global(initGlobalConfig(projectPath));
        // 3、包配置
        String moduleName = scanner("模块名");
        String[] tableNames = scanner("表名，多个英文逗号分割").split(",");
        autoGenerator.packageInfo(initPackageConfig(projectPath,moduleName));
        // 4、策略配置
        autoGenerator.strategy(initStrategyConfig(tableNames));
        // 5、执行
        autoGenerator.execute(new VelocityTemplateEngine());

    }

    private static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(("请输入" + tip + "："));
        if (scanner.hasNext()) {
            String next = scanner.next();
            if (StrUtil.isNotEmpty(next)) {
                return next;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "！");
    }

    /**
     * 初始化数据源配置
     */
    private static DataSourceConfig initDataSourceConfig() {
        Props props = new Props("generator.properties");
        String url = props.getStr("dataSource.url");
        String username = props.getStr("dataSource.username");
        String password = props.getStr("dataSource.password");
        return new DataSourceConfig.Builder(url,username,password)
                .dbQuery(new MySqlQuery())
                .build();
    }
    /**
     * 初始化全局配置
     */
    private static GlobalConfig initGlobalConfig(String projectPath) {
        return new GlobalConfig.Builder()
                //代码生成路径
                .outputDir(projectPath + "/src/main/java")
                .author("xiliu")
                .disableOpenDir()
                .enableSwagger()
                //重新生成时文件是否覆盖
                .fileOverride()
                //定义生成的实体类中日期类型
                .dateType(DateType.ONLY_DATE)
                .build();
    }
    /**
     * 初始化包配置
     */
    private static PackageConfig initPackageConfig(String projectPath, String moduleName) {
        Props props = new Props("generator.properties");
        return new PackageConfig.Builder()
                .moduleName(moduleName)
                .parent(props.getStr("package.base"))
                .entity("entity")
                .pathInfo(Collections.singletonMap(OutputFile.mapperXml, projectPath + "/src/main/resources/mapper/" + moduleName))
                .build();
    }
    /**
     * 初始化策略配置
     */
    private static StrategyConfig initStrategyConfig(String[] tableNames) {
        StrategyConfig.Builder builder = new StrategyConfig.Builder();
        builder.entityBuilder()
                .naming(NamingStrategy.underline_to_camel)
                .columnNaming(NamingStrategy.underline_to_camel)
                .enableLombok()
                .formatFileName("%s")
                .mapperBuilder()
                .enableBaseResultMap()
                .formatMapperFileName("%sMapper")
                .formatXmlFileName("%sMapper")
                .serviceBuilder()
                .formatServiceFileName("%sService")
                .formatServiceImplFileName("%sServiceImpl")
                .controllerBuilder()
                .enableRestStyle()
                .formatFileName("%sController");
        //当表名中带*号时可以启用通配符模式
        if (tableNames.length == 1 && tableNames[0].contains("*")) {
            String[] likeStr = tableNames[0].split("_");
            String likePrefix = likeStr[0] + "_";
            builder.likeTable(new LikeTable(likePrefix));
        } else {
            builder.addInclude(tableNames);
        }
        return builder.build();
    }
}
